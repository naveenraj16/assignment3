
import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class MeetingCount implements MeetingCountService{
	
	final static String BASE_URL = "http://eavesdrop.openstack.org/meetings/";

	public ArrayList<String> getMeetingCounts(String project) {
		ArrayList<String> result = new ArrayList<String>();
		Document doc;
		
		try {
			
	 
			// need http protocol
			doc = Jsoup.connect(BASE_URL+project+"/").get();
	 
	 
			// get all links
			Elements outerLinks = doc.select("a[href]");
			for(int i = 5; i < outerLinks.size(); i++){
				Element ele = outerLinks.get(i); 
				String year = ele.attr("href");
				//take off the slash at end
				year = year.substring(0,year.length()-1);
				doc = Jsoup.connect(BASE_URL+project+"/"+year+"/").get();
				Elements innerLinks = doc.select("a[href]");
				int count = 0;
				for(int j = 5; j < innerLinks.size(); j++){
					if(innerLinks.get(j).attr("href").endsWith(".log.html"))
						count++;
				}
				result.add(year+":"+count);
			}
	 
		} catch (IOException e) {
			//e.printStackTrace();
			return result;
		}
		return result;
	}
}

