

import java.util.ArrayList;


public interface MeetingCountService {
	
	ArrayList<String> getMeetingCounts(String project);
}

