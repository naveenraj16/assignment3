
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class MeetingCountController {
	
	final static String BASE_URL = "http://eavesdrop.openstack.org/meetings/";
	private MeetingCountService meetingCountService;
	
public MeetingCountController() {
}
	
public MeetingCountController(MeetingCountService mService) {
	this.meetingCountService = mService;
}


@ResponseBody
@RequestMapping(value = "/")
public String printMessage(){ 
	return "Please visit <a href=\"http://localhost:8080/assignment3/meetings?project=barbican\">see project barbican counts</a>";
}


@ResponseBody
@RequestMapping(value = "/meetings", params = {"project"}, method=RequestMethod.GET)
public String getMeetingCount(@RequestParam("project") String project) throws IOException {
	String url = BASE_URL+project;
	URL u = new URL(url);
	HttpURLConnection con = (HttpURLConnection)u.openConnection();
	//404 response code if it doesn't exist
	if(con.getResponseCode() == 404)
		return "Unknown Project "+ project;
	ArrayList<String> a = meetingCountService.getMeetingCounts(project);
	String answer = "<P>Meeting Count for "+project+"<P><table border=2><tr><th>Year</th><th>Count</th></tr>";
	for(int i = 0; i < a.size(); i++){
		int index = a.get(i).indexOf(":");
		String year = a.get(i).substring(0,index);
		String count = a.get(i).substring(index+1);
		answer += "<tr><td align=\"right\">"+year+"</td><td align=\"right\">"+count+"</td></tr>";
	}
	return answer+"</table>";
}

public MeetingCountService getMeetingCountService(){
	return meetingCountService;
}

public void setMtgCount(MeetingCountService mcs) {
    this.meetingCountService = mcs;

}

}


