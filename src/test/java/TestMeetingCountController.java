import java.io.IOException;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;



public class TestMeetingCountController {
	
	MeetingCountController mcc = new MeetingCountController();
	MeetingCountService mockMcs = null;
	
@Before
public void setup(){
	mockMcs = mock(MeetingCountService.class);
	mcc.setMtgCount(mockMcs);
}

@Test
public void alwaysPasses(){
	
}

@Test
public void test1() throws IOException{
	ArrayList<String> a = new ArrayList<String>();
	a.add("2017:5");
	when(mockMcs.getMeetingCounts("barbican")).thenReturn(a);
	String expectedResult = "<P>Meeting Count for barbican<P>"
			+ "<table border=2><tr><th>Year</th><th>Count</th></tr>"+
			"<tr><td align=\"right\">2017</td><td align=\"right\">5</td></tr></table>";
	String actualResult = mcc.getMeetingCount("barbican");
	assertEquals(expectedResult, actualResult);
}

@Test
public void test2() throws IOException{
	ArrayList<String> a = new ArrayList<String>();
	a.add("2017:5");
	when(mockMcs.getMeetingCounts("barbica")).thenReturn(a);
	String expectedResult = "Unknown Project barbica";
	String actualResult = mcc.getMeetingCount("barbica");
	assertEquals(expectedResult, actualResult);
}

}
