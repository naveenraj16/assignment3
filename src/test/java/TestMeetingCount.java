import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;


public class TestMeetingCount {

	MeetingCount mc = null;
	
@Before
public void setup(){
	mc = new MeetingCount();
}

@Test
public void testMeetingCount1(){
	ArrayList<String> expectedResult = new ArrayList<String>();
	expectedResult.add("2013:1");
	expectedResult.add("2014:43");
	expectedResult.add("2015:9");
	ArrayList<String> result = mc.getMeetingCounts("barbican");
	
	assertEquals(expectedResult,result);
}

@Test
public void testMeetingCount2(){
	ArrayList<String> expectedResult = new ArrayList<String>();
	expectedResult.add("2013:7");
	expectedResult.add("2014:34");
	expectedResult.add("2015:9");
	ArrayList<String> result = mc.getMeetingCounts("oslo");
	
	assertEquals(expectedResult,result);
}

@Test
public void testMeetingCount3(){
	ArrayList<String> expectedResult = new ArrayList<String>();
	ArrayList<String> result = mc.getMeetingCounts("barbica");
	
	assertEquals(expectedResult,result);
}
	
	
}
